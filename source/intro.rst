Introduction
************

Le but est de créer trois réseaux séparés par des VLANs hébergeant des services différents et mettre en place des règles de restrictions à certains d'entre eux.

.. image:: images/etude_de_cas_02.svg
