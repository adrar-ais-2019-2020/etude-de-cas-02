Configuration du réseau
=======================

Dans le cadre des révisions, nous utilisons du matériel CISCO pour la partie réseau. Nous commencerons par revoir les commandes.

Dans un premier temps il est important de rappeler que si on ne sauvegarde pas la configuration en cours d'exécution, elle sera écrasé au prochain redémarrage de la machine.

Pour sauvegarder la configuration on utilise les commandes suivantes :

.. code::
 
  Switch>en
  Switch#copy running-config startup-config 
  Destination filename [startup-config]? 
  Building configuration...
  [OK]


Switch
------

Création et assignation d'un VLAN
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La création de VLAN s'effectue avec les commandes suivantes :

.. code:: 
  
  Switch>en
  Switch#conf t
  Switch(config)#vlan 10
  Switch(config-vlan)#name vlan10
  Switch(config-vlan)#exit

Pour assigner un VLAN à toute une rangée de port, on utilise les commandes suivantes :

.. code::

  Switch>en
  Switch#conf t
  Switch(config)#interface range fastEthernet 0/1-10
  Switch(config-if-range)#switchport access vlan 10
  Switch(config-if-range)#no shutdown 
  Switch(config-if-range)#exit


Activation du mode Trunk
^^^^^^^^^^^^^^^^^^^^^^^^

Pour configurer une interface qui laisse passer les VLAN vers le routeur, on utilise les commandes suivantes :

.. code::

  Switch#conf t
  Enter configuration commands, one per line.  End with CNTL/Z.
  Switch(config)#interface GigabitEthernet 0/1
  Switch(config-if)#switchport mode trunk
  Switch(config-if)#switchport trunk allowed vlan 10,20,90
  Switch(config-if)#no shutdown
  Switch(config-if)#end

Configuration du routeur
------------------------

Création et configuration des VLAN
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Afin d'afficher la liste des vlan on utilise la commande suivante :

.. code::

  Router>en
  Router#show vlan

  VLAN Name                             Status    Ports
  ---- -------------------------------- --------- -------------------------------
  1    default                          active    
  1002 fddi-default                     act/unsup 
  1003 token-ring-default               act/unsup 
  1004 fddinet-default                  act/unsup 
  1005 trnet-default                    act/unsup 
  
  VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
  ---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
  1    enet  100001     1500  -      -      -        -    -        0      0
  1002 fddi  101002     1500  -      -      -        -    -        0      0   
  1003 tr    101003     1500  -      -      -        -    -        0      0   
  1004 fdnet 101004     1500  -      -      -        ieee -        0      0   
  1005 trnet 101005     1500  -      -      -        ibm  -        0      0   
  
  Remote SPAN VLANs
  ------------------------------------------------------------------------------
  
  
  Primary Secondary Type              Ports
  ------- --------- ----------------- ------------------------------------------

Pour gérer les VLAN, il est nécessaire de crée des sous interfaces, dans un premier temps on active l'interface cible sans lui assignée d'adresse IP :

.. code::

  Router#conf t
  Enter configuration commands, one per line.  End with CNTL/Z.
  Router(config)#interface fastEthernet 0/0
  Router(config-if)#no shutdown 
  
  Router(config-if)#
  %LINK-5-CHANGED: Interface FastEthernet0/0, changed state to up
  
  %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
  Router(config-if)#exit

On crée une sous interface dans laquelle on active l'encapsulation 802.1Q pour le support des VLAN. L'étape suivante consiste à lui assigner un adresse IP.

.. code::

  Router(config)#interface fastEthernet 0/0.10
  Router(config-subif)#
  %LINK-5-CHANGED: Interface FastEthernet0/0.10, changed state to up
  
  %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0.10, changed state to up
  
  Router(config-subif)#encapsulation dot1Q 10
  Router(config-subif)#ip address 192.168.1.254 255.255.255.0
  Router(config-subif)#ip helper-address 192.168.90.1
  Router(config-subif)#no shutdow
  Router(config-subif)#exit

.. note::
  L'usage de la commande ip helper-adresse fait en sorte de contenir les Broadcast DHCP vers l'adresse du serveur cible. Le routeur reagis comme un relay DHCP sur l'interface et redirige les requete vers la bonne machine.

Le routage dynamique
^^^^^^^^^^^^^^^^^^^^

La configuration du routage dynamique avec le protocole RIP est effectuée grâce au commandes suivantes :

.. code::

  Router(config)#router rip
  Router(config-router)#net
  Router(config-router)#version 2
  Router(config-router)#network 192.168.1.0
  Router(config-router)#network 192.168.2.0
  Router(config-router)#network 192.168.90.0
  Router(config-router)#end

Chaque ajout de réseau permettra au routeur de le publié autres routeur dont le protocole RIP est activé.

Les Access Control List (ACL)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: shell

  Router(config)#ip access-list extended <rule-name>
  Router(config-ext-nacl)#permit tcp 192.168.10.0 0.0.0.255 host 192.168.30.1 eq 80
  Router(config-ext-nacl)#permit tcp <authorised network> <authorized network reverse subnet> host <destination ip eq <destination port>
  Router(config-ext-nacl)#deny udp 192.168.20.254 host 192.168.90.1 eq 67
  Router(config-ext-nacl)#deny udp <interface source a bloquer> host <adresse de destination> eq 67
  Router(config-ext-nacl)#exit
  Router(config)#interface fastEthernet 0/0.30
  Router(config-subif)#access-group acces-srv-web out
  Router(config)#show acces-list 
  Router(config)#no ip access-list extended <rule-name>


Les ACL sont appliquer sur l'interface la plus proche du service a fournir.
