Installation et configuration de PostgreSQL
-------------------------------------------

L'installation de PostgreSQL sous OpenSUSE 15.1 est simple, dans un premier temps on installe les paquets :

.. code:: shell

  ~> sudo  zypper install postgresql postgresql-server postgresql-contrib

On rend le service active immédiatement et persistant au démarrage de la machine :

.. code:: shell

  ~> sudo systemctl enable postgresql.service
  ~> sudo systemctl start postgresql.service
  ~> systemctl status postgresql.service
  ● postgresql.service - PostgreSQL database server
     Loaded: loaded (/usr/lib/systemd/system/postgresql.service; enabled; vendor preset: disabl>
     Active: active (running) since Thu 2019-10-17 12:15:37 CEST; 2min 44s ago
    Process: 2332 ExecStart=/usr/share/postgresql/postgresql-script start (code=exited, status=>
    Main PID: 2355 (postgres)
       Tasks: 8 (limit: 4915)
     CGroup: /system.slice/postgresql.service
             ├─2355 /usr/lib/postgresql10/bin/postgres -D /var/lib/pgsql/data
             ├─2356 postgres: logger process   
             ├─2358 postgres: checkpointer process   
             ├─2359 postgres: writer process   
             ├─2360 postgres: wal writer process   
             ├─2361 postgres: autovacuum launcher process   
             ├─2362 postgres: stats collector process   
             └─2363 postgres: bgworker: logical replication launcher

Il est nécessaire lors de la première connexion de crée un compte avec les droit super utilisateur. Pour des raisons de simplicité, on crée un utilisateurs comprenant le même nom que l'identifiant de l'administrateur. Exemple, si l'utilisateur est toto, alors on crée un rôle toto).

.. code:: shell

  user~> sudo su - postgres
  postgres~> psql
  postgres=# CREATE ROLE <username> WITH PASSWORD '<passwd>';
  postgres=# ALTER ROLE <username> WITH LOGIN;
  postgres=# ALTER ROLE <username> WITH SUPERUSER;
  postgres=# CREATE DATABASE <username> TEMPLATE template0;
  postgres=#\q
  postgres~> exit

Il est maintenant possible de ce connecter à PostgreSQL juste en tapant la commande :code:`psql`, mais la configuration actuelle ne demande pas de mot de passe. Pour obliger l'utilisateur a demander un mot de passe on édite le fichier :emphasis:`/var/lib/pgsql/data/pg_hba.conf` :

.. code::

  # On remplace la ligne :
  local   all             all                                     peer
  # Par la ligne :
  local   all             all                                     md5

Il faut maintenant redémarrer le service pour prendre en compte les changements :

.. code:: shell

  ~> sudo systemctl restart postgresql.service


Si l'on fait une tentative de connexion :

.. code:: shell

  user~> psql 
  Password: 
  psql (10.10)
  Type "help" for help.

  rollniak=# \q
  user~> psql 

Nous pouvons maintenant nous connecte au serveur avec des droits d'administrateur.
